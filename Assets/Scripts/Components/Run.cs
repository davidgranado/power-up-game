﻿using UnityEngine;

[RequireComponent (typeof(Rigidbody))]
public class Run : MonoBehaviour {
	public float maxSpeed = 15f;
	public float speed = 25.0f;
	public float drag = 5f;

	private float _magnitude = 0;
	private Rigidbody _rb;

	public int Direction {
		get {
			return _magnitude > 0 ? 1 : -1;
		}
	}


	// Use this for initialization
	void Start () {
		_rb = GetComponent<Rigidbody>();
	}

	// Update is called once per frame
	void FixedUpdate () {
		SetSpeed();
	}

	public void SetSpeedVector(float magnitude) {
		_magnitude = Mathf.Clamp(magnitude, -1, 1);
	}

	void SetSpeed() {
		_rb.velocity = new Vector3(
			Mathf.Clamp(
				_rb.velocity.x + Time.deltaTime * (
					(
						_magnitude * speed
					) - (
						drag * _rb.velocity.x
					)
				),
				-maxSpeed,
				maxSpeed
			),
			_rb.velocity.y,
			0
		);
	}
}
