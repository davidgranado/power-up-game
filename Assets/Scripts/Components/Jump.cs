﻿using UnityEngine;

[RequireComponent (typeof(Rigidbody))]
public class Jump : MonoBehaviour {
	public float force = 5.0f;

	private bool _jumpQueued = false;
	private Rigidbody _rb;

	// Use this for initialization
	void Start () {
		_rb = GetComponent<Rigidbody>();
	}

	// Update is called once per frame
	void Update () {
		if(_jumpQueued) {
			_jumpQueued = false;
			ApplyJump();
		}
	}

	public void QueueJump() {
		_jumpQueued = true;
	}

	bool JumpIsQueued{
		get {
			return _jumpQueued;
		}
	}

	void ApplyJump() {
		_rb.AddForce(0, force, 0, ForceMode.VelocityChange);
	}
}
