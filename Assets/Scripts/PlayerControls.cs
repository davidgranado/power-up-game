using UnityEngine;
using System.Collections;

[
	RequireComponent (typeof(Run)),
	RequireComponent (typeof(Jump)),
	RequireComponent (typeof(Flight))
]
public class PlayerControls : MonoBehaviour {
	public bool isFlying = false;

	private bool isGrounded;
	private Flight _flight;
	private Run _run;
	private Jump _jump;

	// Use this for initialization
	void Start () {
		_flight = GetComponent<Flight>();
		_run = GetComponent<Run>();
		_jump = GetComponent<Jump>();
	}

	// Update is called once per frame
	void Update () {
		if (Input.GetButtonUp ("Fire3")) {
			isFlying = !isFlying;
		}
	}

	void FixedUpdate() {
		ApplyForces();
	}

	void OnCollisionEnter(Collision col) {
		if(!isGrounded && col.collider.tag == "Ground") {
			isGrounded = true;
		}
	}

	void OnCollisionExit(Collision col) {
		if(isGrounded && col.collider.tag == "Ground") {
			isGrounded = false;
		}
	}

	private void ApplyForces() {
		_run.SetSpeedVector(Input.GetAxis("Horizontal"));

		if (!isFlying && isGrounded && Input.GetButton ("Jump")) {
			_jump.QueueJump();
		}
	}
}
